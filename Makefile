HOST="web_csp.estada.ch@yeti.estada.ch"
DB_NAME="reports.db"

sync: backup deploy
	# done

backup:
	scp ${HOST}:${DB_NAME} .

upload_local_db:
	scp ${DB_NAME} ${HOST}:

deploy: build_release deploy_direct
	# done

deploy_direct:
	ssh ${HOST} -t "systemctl --user stop cspec.service"
	scp -r static/ ${HOST}:
	rsync -av target/release/csp_endpoint_collector ${HOST}:
	make systemd_restart

reset_local_db:
	rm -f ${DB_NAME}
	diesel setup

reset_remote_db: reset_local_db
	scp -r ${DB_NAME} ${HOST}:
	make systemd_restart

systemd_setup:
	ssh ${HOST} "mkdir -p ~/.config/systemd/user/"
	scp cspec.service ${HOST}:~/.config/systemd/user/cspec.service
	ssh ${HOST} -t "systemctl --user daemon-reload ; systemctl --user restart cspec.service ; systemctl --user enable cspec.service ; systemctl --user status cspec.service"

systemd_restart:
	ssh ${HOST} -t "systemctl --user restart cspec.service ; systemctl --user status cspec.service"

build_release:
	cargo build --release

build_in_container:
	docker pull rust:latest
	docker run --rm --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -w /usr/src/myapp rust:latest cargo build --release
	chown -R "$$(id -u)":"$$(id -g)" target/release/

ssh:
	ssh ${HOST}

list_users:
	echo 'select * from users;' | sqlite3 reports.db 

demo_report:
	echo '{"csp-report":{"blocked-uri":"inline","column-number":13,"document-uri":"https://art.estada.ch/htdocs/","line-number":3453,"original-policy":"default-src '\''none'\''; connect-src '\''self'\'' wss://api.estada.ch; font-src '\''self'\''; img-src '\''self'\'' data: https://*.tile.openstreetmap.org/; script-src '\''self'\'' '\''unsafe-inline'\''; style-src '\''self'\''; report-uri https://csp.estada.ch/report","referrer":"","source-file":"https://art.estada.ch/htdocs/angular.js","violated-directive":"style-src"}}' | curl -X POST -H "Content-Type: application/json" --data-binary @- -v http://localhost:7878/report/1234-5678
	@echo

demo_fetch_reports:
	# /reports/:user/:customer
	curl -v http://localhost:7878/reports/XXXXXXXX-YYYYYYY/1234-5678 | jq
	curl -v http://localhost:7878/reports/AAAAAA-BBBBB/OOOOOOO | jq
	curl -v http://localhost:7878/reports/AAAAAA-BBBBB/1234-5678 #| jq
	@echo

demo_check_live_policy:
	curl -v http://localhost:7878/check/XXXXXXXX-YYYYYYY/1
	@echo

demo_analiser_ui:
	curl -v http://localhost:7878/analise/XXXXXXXX-YYYYYYY/
	@echo