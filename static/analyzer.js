const Analyzer = (function() {
    const USER_ID = location.pathname.split("/").filter(p => p !== "")[1];
    const REPORTS_BASE_URL = `/reports/${USER_ID}/`;
    const CHECK_BASE_URL = `/check/${USER_ID}/`;
    const CUSTOMERS_SELECTOR = "#customer_panel > *";
    const TABLE_CONTAINER_SELECTOR = "p#table";
    
    // the data storages
    let customers = [];
    let active_customer = undefined;
    let reports = {};
    let last_updated = {};
    // all the table data
    let table = undefined
        , table_head = undefined
        , table_after_status = undefined;
    let filter = undefined
        , filter_string = ''
        , filter_results_stats_field = undefined;
    let filter_timeout = undefined
        , typing_timeout = undefined;
    let refresh_button = undefined;
    let modal_box = undefined;
    
    return {
        init: init,
        get: get,
        update_dom: update_dom,
        update_filter: update_filter,
        refresh_button: refresh_button,
        refresh_button_handler: refresh_button_handler,
    };
    
    async function init() {   
        Array.prototype.forEach.call(document.querySelectorAll(CUSTOMERS_SELECTOR), lb => {
            const checkbox = lb.querySelector('input');
            const api_key = checkbox.value;
            const name = lb.innerText;

            checkbox.checked = false;
            const args = {
                name: name,
                api_key: api_key,
                checkbox: checkbox,
                load_all_results: false,
            };
            checkbox.addEventListener("change", setup_customers_change_listener(args))

            customers.push(args);
        });
        
        const table_container = document.querySelector(TABLE_CONTAINER_SELECTOR);
        table_container.innerHTML = `
            <p>
                <bold>Filter:</bold> document-url:
                <input id="url-filter" type="search" value="" placeholder="Search here" />
                <span id="url-filter-stats"></span>
                <button id="refresh">Loading ...</button>
            </p>
            <div id="modal-anchor">
                <div id="modal-box" class="hidden"></div>
                <table><thead></thead><tbody></tbody></table>
                <div id="status-bar"></div>
            </div>
        `;
        filter = table_container.querySelector('#url-filter');
        filter_results_stats_field = table_container.querySelector('#url-filter-stats');
        modal_box = table_container.querySelector('#modal-box');
        refresh_button = table_container.querySelector('#refresh');
        table = table_container.querySelector('tbody');
        table_head = table_container.querySelector('thead');
        table_after_status = table_container.querySelector('#status-bar');
        
        filter.addEventListener('keyup', update_filter);
        refresh_button.addEventListener('click', refresh_button_handler);
        table.addEventListener('click', table_click_handler);
        
        handle_hash_change();
        //window.onpopstate = handle_pop_state;
        window.addEventListener('hashchange', handle_hash_change, false);

        await render_active_customer();
    }

    function setup_customers_change_listener(opts) {
        return function(_ev) {
            const self = this;
            const checked = opts.checkbox.checked;

            if (checked === true) {
                customers.filter(c => c.checkbox !== self).forEach(c => c.checkbox.checked = false);
                update_active_customer(opts);
            } else {
                if (active_customer === opts) {
                    update_active_customer(undefined);
                }
            }
        }
    }

    function update_active_customer(next) {
        //console.log(['cb', next.name])

        table_after_status.innerHTML = 'Changeing Customer';
        active_customer = next;
        render_active_customer();
    }

    async function render_active_customer() {
        if (active_customer === undefined) {
            table_after_status.innerHTML = '<p>Select a customer to view their data</p>';
            //return;
            table.classList.add('hidden');
        } else {
            await get();
            table.classList.remove('hidden');
        }

        table_after_status.innerHTML = '';
        
        update_head();
        update_filter();
        update_dom();
    }
    
    async function get(refresh) {
        if (active_customer === undefined) {
            refresh_button.innerText = 'Update (select customer)';
            return ;
        }
        if (reports[active_customer.api_key] === undefined || reports[active_customer.api_key].length === 0 || refresh) {
            table_after_status.innerHTML = '<p>fetching reports of customer, please wait ...</p>';

            const all = active_customer.load_all_results ? '?all=true' : '';
            const data = await fetch(REPORTS_BASE_URL + active_customer.api_key + all);
            const json = await data.json();
            console.log(['get', json]);
            reports[active_customer.api_key] = json.map(report => {
                const headers = report.referer === '<>' ? [] : JSON.parse(report.referer);
                return {
                    id: report.id,
                    timestamp: new Date(report.timestamp),
                    headers: headers,
                    report: JSON.parse(report.json)["csp-report"],
                }
            })
            .reverse();
            
            // Update refresh button
            const now = new Date();
            last_updated[active_customer.api_key] = now;
        }

        refresh_button.innerHTML = `Refresh data (last: ${format_timestamp(last_updated[active_customer.api_key])})`;
        
        modal_box.classList.add('hidden');
        //return reports[active_customer.api_key];
    }
    
    function update_filter() {
        const new_filter_string = filter.value.trim().toLowerCase();
        
        if (new_filter_string === filter_string) {
            return;
        }
        filter_string = new_filter_string;
        
        if (typeof filter_timeout !== 'undefined') {
            clearTimeout(filter_timeout);
        }
        filter_timeout = setTimeout(_ => {
            update_dom();
            filter_timeout = undefined;
        }, 666);
        
        
        if (typeof typing_timeout !== 'undefined') {
            clearTimeout(typing_timeout);
        }
        typing_timeout = setTimeout(_ => {
            location.hash = `#q=${filter_string}`;
            typing_timeout = undefined;
        }, 2500);
    }
    
    function format_timestamp(ts) {
        return `
            ${ts.getFullYear()}-${pad(ts.getMonth() + 1)}-${pad(ts.getDate())}
            ${pad(ts.getHours())}:${pad(ts.getMinutes())}:${pad(ts.getSeconds())}
        `;
    }
    
    function pad(s) {
        const pad = '00';
        return (pad + s).slice(-pad.length)
    }
    
    function update_dom() {
        filter_timeout = undefined;
        table_head.classList.add("hidden");
        if (active_customer === undefined) {
            return;
        }
        let html = ''
            , n_results = 0;

        const ff = build_filter_function(filter_string);
        
        reports[active_customer.api_key]
        .filter(ff)
        .forEach(r => {
            html += `
                <tr data-id="${r.id}">
                    <td class="hidden">${r.id}</td>
                    <td>${format_timestamp(r.timestamp)}</td>
                    <td><a href="${r.report["document-uri"]}" target="_blank">${r.report["document-uri"]}</a></td>
                    <td>
                        ${link_if_https(r.report["blocked-uri"])}
                        <br><hr><b>source-file:</b>
                        ${link_if_https(r.report["source-file"])}
                    </td>
                    <td>${format_violated_directive(r.report["violated-directive"])}</td>
                    <td><pre>${format_policy(r.report["original-policy"], r.report["violated-directive"])}</pre></td>
                    <td>
                        <pre>${format_current_policy(r.report["current-policy"], r.report["original-policy"], r.report["violated-directive"])}</pre>
                        <button>fetch</button>
                    </td>
                </tr>
            `;
            n_results += 1;
        });
        table.innerHTML = html;

        if (filter_string.trim() === '') {
            // No filter applied
            filter_results_stats_field.innerHTML =
                `${reports[active_customer.api_key].length} reports loaded`;
        } else {
            filter_results_stats_field.innerHTML =
                `Found <bold>${n_results}</bold> in ${reports[active_customer.api_key].length}`;
            if (n_results === 0) {
                table_after_status.innerHTML =
                    `<p>No results found, searched ${reports[active_customer.api_key].length} reports</p>`;
            } else {
                table_after_status.innerHTML =
                    `<p>Found ${n_results}, searched ${reports[active_customer.api_key].length} reports</p>`;
                table_head.classList.remove("hidden");
            }
        }

        if (active_customer.load_all_results === false) {
            table_after_status.innerHTML += 
                `<button id="load-older-results">Load older results</button>`;
            table_after_status.querySelector("#load-older-results")
                .addEventListener("click", function(_ev) {
                    active_customer.load_all_results = true;
                    reports[active_customer.api_key] = [];
                    render_active_customer();
                });
        }

        
        table.querySelectorAll('button')
        .forEach(bt => bt.addEventListener('click', fetch_current_policy_handler));

        /*if (prevent_push_state) {
            const state = {
                filter_string: filter_string,
            }
                , title = ''
                , url = `#q=${filter_string}`;
            history.pushState(state, title, url);
        }*/
    }

    function link_if_https(url) {
        try {
            const _u = new URL(url);
            return `<a href="${url}" target="_blank">${url}</a>`;
        } catch (_e) {
            return `<span>${url}</span>`;
        }
    }
    
    function handle_pop_state(event) {
        console.log('handle_pop_state!');
        const state = event.state;
        if (!state) { return ; }
        
        filter_string = state.filter_string;
        filter.value = filter_string;
        
        update_dom();
    }
    function handle_hash_change() {
        console.log('handle_hash_change');
        
        const new_filter_string = decodeURIComponent(location.hash.split('q=')[1] || '');
        
        if (new_filter_string === filter_string && new_filter_string === filter.value) {
            return;
        }
        
        filter_string = new_filter_string;
        filter.value = filter_string;
        
        update_dom();
    }
    
    function update_head() {
        let html = '';
        //reports.forEach(r => {
            html += `
                <tr>
                    <th class="hidden">id</th>
                    <th>timestamp</th>
                    <th>document-uri</th>
                    <th>blocked-uri<br>source-file</th>
                    <th>violated-directive</th>
                    <th>original-policy</th>
                    <th>current-policy</th>
                </tr>
            `;
        //});
        table_head.innerHTML = html;
    }

    function format_current_policy(current_policy, recorded_policy, violated_directive) {
        if (current_policy === undefined || recorded_policy === undefined) {
            return '';
        }
        if (current_policy.trim() === recorded_policy.trim()) {
            return "SAME POLICY";
        }
        
        return format_policy(current_policy, violated_directive);
    }
    
    function format_policy(policy, violated_directive) {
        violated_directive = violated_directive.split(' ')[0];
        
        return (policy || '')
            .trim().split(";")
            .map(part => {
                let p = part.trim();
                if (p.indexOf(violated_directive) === 0) {
                    p = `<b>${p}</b>`;
                }
                return p;
            })
            .join(";\n");
    }
    
    function format_violated_directive(vd) {
        return `<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/${vd}" target="_blank">${vd}</a>`;
    }
    
    async function refresh_button_handler() {
        refresh_button.innerHTML = 'Updating ...';
        await get(true);
    }

    async function fetch_current_policy_handler(ev) {
        const self = ev.target;

        ev.preventDefault();
        self.innerHTML = 'Updating ...';

        let node = ev.target;
        while (node.nodeName !== 'TR') {
            node = node.parentNode;
        }
        const id = parseInt(node.dataset["id"]);
        const report = reports[active_customer.api_key].find(r => r.id === id);

        // TODO
        const response = await fetch(CHECK_BASE_URL + id, {
            //method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            //body: JSON.stringify({'document-uri': report.report['document-uri']}),
        });
        const current_policy = JSON.parse(await response.text());
        report.report["current-policy"] = current_policy.policy;

        self.innerHTML = "fetch";
        const output = self.parentNode.querySelector('pre');
        output.innerHTML = format_current_policy(current_policy.policy, report.report["original-policy"], report.report["violated-directive"]);

    }
    
    function table_click_handler(ev) {
        let node = ev.target;
        const tag = node.nodeName; // evt. tagName?
        if (tag === 'A' || tag === 'BUTTON') { return }
        
        while (node.nodeName !== 'TR') {
            node = node.parentNode;
        }
        
        const last_id = parseInt(modal_box.dataset['id']);
        const id = parseInt(node.dataset["id"]);
        
        if (id === last_id) {
            modal_box.classList.add('hidden');
            modal_box.dataset['id'] = undefined;
        } else {
            modal_box.dataset['id'] = id;
            const report = reports[active_customer.api_key].find(r => r.id === id);
            
            modal_box.classList.remove('hidden');
            modal_box.style.left = '0.5rem'; // ev.layerX + 'px';
            modal_box.style.top  = ev.layerY + 6 + 'px';
            
            set_modal_box(report);
        }
        
        ev.preventDefault();
    }
    
    function set_modal_box(report) {
        modal_box.innerHTML = '<pre></pre> <pre></pre>';
        const report_pre = modal_box.children[0];
        const headers_pre = modal_box.children[1];
        
        format_array(report_pre, report.report, "Report", ['blocked-uri', 'document-uri', 'line-number', 'column-number', 'effective-directive', 'violated-directive', 'original-policy', 'source-file']);
        format_array(headers_pre, report.headers, "Headers", ['referer', 'sec-fetch-mode', 'sec-fetch-site', 'user-agent']);
    }
    
    function format_array(pre, data, title, selection) {
        let html = `<h3>${title}:</h3>`;
        selection.forEach(key => {
            html += `\n<bold>${key}: </bold>${data[key] || ''}`;
        });
        pre.innerHTML = html;
    }
    
    /// a bc -> Is('a').And('bc')
    /// a! -> Is('a!')
    /// "a bc" -> Is('a bc')
    /// !bla -> Not('bla')
    /// |bli -> Or('bli')
    function build_filter_function(filter_string_input) {
        const filter_string = filter_string_input.trim();
        
        function And(token, ft) { this.token = token; this.prev_filter = ft; }
        function Or (token, ft) { this.token = token; this.prev_filter = ft; }
        function Not(token, ft) { this.token = token; this.prev_filter = ft; }
        function match_prev(s) {
            if (this.prev_filter === undefined) { return true; }
            return this.prev_filter.match(s);
        }
        And.prototype.match_prev = match_prev;
        Or .prototype.match_prev = match_prev;
        Not.prototype.match_prev = match_prev;
        And.prototype.match = function(s) { return this.match_prev(s) && s.indexOf(this.token) !== -1 };
        Or .prototype.match = function(s) { return this.match_prev(s) || s.indexOf(this.token) !== -1 };
        Not.prototype.match = function(s) { return this.match_prev(s) && s.indexOf(this.token) === -1 };
        
        
        var token = '';
        var active_quote = undefined, escape_next = false, skip_leading_spaces = false;
        var filter_tree = undefined;
        var next_token_type = And;
        
        for (const c of filter_string) {
            if (skip_leading_spaces) {
                if (c === ' ') {
                    continue;
                } else {
                    skip_leading_spaces = false;
                }
            }
            if (c === '"' || c === "'") {
                if (escape_next === false) {
                    if (active_quote === undefined) {
                        active_quote = c;
                        continue;
                    } else {
                        active_quote = undefined;
                        continue;
                    }
                }
            }
            if (token === '') {
                if (c === '!') {
                    next_token_type = Not;
                    continue;
                } else if (c === '|') {
                    next_token_type = Or;
                    continue;
                }
            }
            if (escape_next === false && c === '\\') {
                escape_next = true;
                continue;
            }
            if (c === ' ' && (escape_next === false || active_quote === undefined)) {
                handle_token(token);
                token = '';
                skip_leading_spaces = true;
            } else {
                token += c;
            }
        }
        if (filter_tree === undefined || token !== '') {
            handle_token(token);
        }
        
        //return r => r.report["document-uri"].toLowerCase().indexOf(filter_string) !== -1;
        return r => {
            const s = r.report["document-uri"].toLowerCase();
            return filter_tree.match(s);
        };
        
        function handle_token(token) {
            filter_tree = new next_token_type(token, filter_tree);
        }
    }
})();

window.addEventListener('load', Analyzer.init());

//export { Analyzer };
