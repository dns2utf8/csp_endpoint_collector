#!/usr/bin/env python3

# check types run: mypy generate_up_down_sql.py 

import os
from typing import Callable, Iterator, Union, Optional, List, Dict

schema_path = "src/schema.rs"
output_path = "migrations/2021-02-18-111334_create_tables/"

def save(name: str, content: str):
    with open(output_path + name, "w") as fd:
        fd.write(content)

class Table:
    name: str
    indexes: str
    columns: Dict
    
    def __init__(self, name, indexes, columns):
        self.name = name
        self.indexes = indexes
        self.columns = columns

def up(schema: List[Table]) -> str:
    # CREATE TABLE users (
    #   id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    #   name VARCHAR NOT NULL,
    #   hair_color VARCHAR
    # );
    r = ""
    for table in schema:
        columns = ""
        log("up", "DD: {}".format(table.columns))
        for col_name, attributes in table.columns.items():
            columns += "    `{}` {} NOT NULL,\n".format(col_name, attributes)
        columns = columns[:-2] + "\n"
        r += "CREATE TABLE IF NOT EXISTS `{}` (\n{});\n".format(table.name, columns)
    return r

def down(schema: List[Table]) -> str:
    r = ""
    for table in schema:
        r += "DROP TABLE `{}`;\n".format(table.name)
    return r

def tokenize(text: str) -> List[str]:
    l = []
    #for t in schema.replace("\n", " ").split(" "):
    tmp = ""
    for c in text:
        if c == "\r":
            pass
        elif c == "\n" or c == " ":
            l.append(tmp)
            tmp = ""
        elif c == "(" or c == ")" or c == "{" or c == "}" or c == ",":
            l.append(tmp)
            tmp = ""
            l.append(c)
        else:
            tmp += c
    if tmp != "":
        l.append(tmp.strip())
    
    return l

def parse_block(tokens: Iterator[str], opener="{", closer="}") -> List[str]:
    token = next(tokens)
    if token != opener:
        log("parse_block", "invalid token '{}'".format(token))
        return list(tokens)
        
    scoped = []
    level = 1
    for token in tokens:
        if token == opener:
            level += 1
        if token == closer:
            level -= 1
            if level == 0:
                break
        scoped.append(token)
    log("parse_block", "scoped tokens: {}".format(scoped))
    return scoped

def translate_type(struct: str) -> str:
    if struct == "Float":
        struct = "Float(24)"
    if struct == "Timestamp":
        struct = "DATETIME DEFAULT CURRENT_TIMESTAMP"
    
    return struct.upper()

def parse_columns(tokens: Iterator[str], indexes: List[str]) -> Dict:
    block = parse_block(tokens)
    log("parse_columns", "\n  columns: {}".format(block))
    r = range(int(len(block) / 4))
    scoped = iter(block)
    m = {}
    for _i in r:
        name = next(scoped)
        arrow = next(scoped)
        if arrow != "->":
            log("parse_columns", "expected '->' got {}".format(arrow))
            raise Exception(arrow)
        struct = translate_type( next(scoped) )
        comma = next(scoped)
        if comma != ",":
            log("parse_columns", "expected ',' got {}".format(comma))
            raise Exception(comma)
        
        if name in indexes:
            struct += " PRIMARY KEY AUTOINCREMENT"
        m[name] = struct
    return m

def parse_table(tokens: Iterator[str]) -> Table:
    block = iter(parse_block(tokens))
    name = next(block)
    indexes = parse_block(block, opener="(", closer=")")
    #indexes = next(block)
    log("parse_table", "name: {}, indexes: {}".format(name, indexes))
    columns = parse_columns(block, indexes)
    
    return Table(name, indexes, columns)

def parse_joinable(tokens: Iterator[str]) -> None:
    """
    basically just ignore it for now
    """
    _inner = parse_block(tokens, opener="(", closer=")")
    #log("parse_joinable", _inner)
    semicolon = next(tokens)
    if semicolon != ";":
        log("parse_joinable", "expected ';' got '{}'".format(semicolon))
    #log("parse_joinable", list(tokens))

def parse_schema(schema: str) -> List[Table]:
    # table! {
    #    reports (id) {
    #        id -> Integer,
    #        timestamp -> Timestamp,
    #        json -> Text,
    #        referer -> Text,
    #    }
    #}
    tokens = filter(lambda t: t != "", tokenize(schema))
    #for token in tokenize(schema):
    #    token = token.strip()
    #    if token == "":
    #        continue
    #    tokens.append(token)
    #
    #tokens = iter(tokens)
    
    tables = []
    for token in tokens:
        if token == "table!":
            tables.append(parse_table(tokens))
        elif token == "joinable!":
            parse_joinable(tokens)
        else:
            log("parse_schema", "invalid token : {}".format(token))
    return tables

# From https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-python
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def log(where: str, what: str):
    print("{}{}:{} {}".format(bcolors.BOLD, where, bcolors.ENDC, what))

with open(schema_path, "r") as schema_file:
    os.makedirs(output_path, exist_ok=True)

    schema_content = schema_file.read()

    schema = parse_schema(schema_content)
    
    save("up.sql", up(iter(schema)))
    save("down.sql", down(iter(schema)))
