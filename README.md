# csp endpoint collector

Collect CSP violation reports and store them in a sqlite DB file.

The gotham diesel middleware uses `tokio_threadpool::blocking`, which allows
blocking operations to run without blocking the tokio reactor. Although not true async,
this allows multiple concurrent database requests to be handled, with a default of 100
concurrent blocking operations. For further details see
[tokio_threadpool::blocking documentation](https://docs.rs/tokio-threadpool/0.1.8/tokio_threadpool/fn.blocking.html).

## Useful links

* http://localhost:7878/static/analyzer.html
* `curl -v -H "Content-Type: application/json" -d '{"no-csp-report":23}' 'http://localhost:7878/report'`
* `curl -v -H "Content-Type: application/json" -d 'INVALID_JSON' 'http://localhost:7878/report'`
* `curl 'https://csp.estada.ch/report' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/csp-report' -H 'Origin: https://csp.estada.ch' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw $'{"csp-report":{"blocked-uri":"https://estada.ch/2020/8/25/color_blinder-v032-maintenance-update-released/2020-08-25_color-blinder-maintenance-update/logo_combined.png","column-number":6,"document-uri":"https://csp.estada.ch/static/analyzer.html","line-number":1827,"original-policy":"default-src \'none\'; connect-src \'self\' wss://api.estada.ch; font-src \'self\'; img-src \'self\' data: https://*.tile.openstreetmap.org/; script-src \'self\' \'unsafe-inline\'; style-src \'self\'; report-uri https://csp.estada.ch/report","referrer":"","source-file":"resource://devtools/server/actors/inspector/walker.js","violated-directive":"img-src"}}'`


## Running
You'll need the diesel cli to setup the database and run migrations.
This can be installed with `cargo install diesel_cli`.
See [diesel getting started instructions](http://diesel.rs/guides/getting-started/) for further details.

First, we'll setup the database and run the application.
Then, we'll create and retieve a product from our database via our REST api.

From the `examples/diesel` directory:
```
Terminal 1:
$ DATABASE_URL=products.db diesel database setup
Creating database: products.db
Running migration 2019-04-09-111334_create_products
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.61 secs
     Running `../gotham_diesel_example`
  Listening for requests at http://127.0.0.1:7878

Terminal 2:
$ curl -v http://127.0.0.1:7878
* Connected to 127.0.0.1 (127.0.0.1) port 7878 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:7878
> User-Agent: curl/7.64.0
> Accept: */*
>
< HTTP/1.1 200 OK
< x-request-id: 62d29d71-ab89-4d9e-a91d-77b22ae3c6dc
< content-type: application/json
< content-length: 2
< date: Thu, 11 Apr 2019 13:52:06 GMT
<
* Connection #0 to host 127.0.0.1 left intact
[]%

$ curl -v -H "Content-Type: application/json" -d '{"title":"test","price":1.0,"link":"http://localhost"}' 'http://localhost:7878'
* Connected to localhost (127.0.0.1) port 7878 (#0)
> POST / HTTP/1.1
> Host: localhost:7878
> User-Agent: curl/7.64.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 54
>
* upload completely sent off: 54 out of 54 bytes
< HTTP/1.1 201 Created
< x-request-id: d02d724a-4b88-4aac-9da3-adef60fff258
< content-type: application/json
< content-length: 10
< date: Thu, 11 Apr 2019 13:52:40 GMT
<
* Connection #0 to host localhost left intact
{"rows":1}%

$ curl -v localhost:7878
* Connected to localhost (127.0.0.1) port 7878 (#0)
> GET / HTTP/1.1
> Host: localhost:7878
> User-Agent: curl/7.64.0
> Accept: */*
>
< HTTP/1.1 200 OK
< x-request-id: 28a3cd70-d781-4671-b52f-67d096e38a79
< content-type: application/json
< content-length: 63
< date: Thu, 11 Apr 2019 13:54:10 GMT
<
* Connection #0 to host localhost left intact
[{"id":1,"title":"test","price":1.0,"link":"http://localhost"}]%
```

## License

Licensed under your option of:

* [MIT License](../../LICENSE-MIT)
* [Apache License, Version 2.0](../../LICENSE-APACHE)

## Community
