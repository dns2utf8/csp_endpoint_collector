use crate::{
    models::{Customer, Report, User},
    Repo,
};
use chrono::{Duration, Utc};
use diesel::prelude::*;

pub(crate) async fn get_customer_id(
    repo: &crate::Repo,
    customer: &crate::utils::CustomerIdExtractor,
) -> Result<i32, &'static str> {
    let customer = unsafe { std::mem::transmute::<&String, &'static String>(&customer.customer) };
    use crate::schema::customers::dsl::*;
    let result = repo
        .run(move |conn| {
            customers
                .filter(api_key.eq(customer))
                .load::<Customer>(&conn)
        })
        .await;
    match result {
        Ok(cs) => match cs.len() {
            1 => {
                let c = &cs[0];
                Ok(c.id)
            }
            0 => Err("customer not found"),
            _ => Err("customer not unique"),
        },
        Err(_e) => {
            // TODO log!("{:?}", e)
            Err("database error")
        }
    }
}

pub(crate) async fn get_user(repo: &Repo, user: &String) -> Option<User> {
    let user: &'static String = unsafe { std::mem::transmute(user) };
    use crate::schema::users::dsl::*;
    let result = repo
        .run(move |conn| users.filter(login_key.eq(user)).load::<User>(&conn))
        .await
        .ok()?;
    if result.len() != 1 {
        return None;
    }
    result.get(0).map(|u| u.clone())
}

pub(crate) async fn get_report(repo: &crate::Repo, user: &User, report_id: i32) -> Option<Report> {
    use crate::schema::reports::dsl::*;
    use crate::schema::users_customers::dsl::{
        //customer_id as uc_customer_id,
        user_id as uc_user_id,
        users_customers,
    };

    let uid = user.id;

    // `select reports.* from reports INNER JOIN users_customers ON users_customers.customer_id=reports.customer_id WHERE users_customers.user_id = 1 AND reports.id = 1;`
    let result = repo
        .run(move |conn| {
            // reports.filter(id.eq(report_id)).load::<Report>(&conn))
            let query = reports
                //.inner_join(users_customers.on(uc_customer_id))
                .inner_join(users_customers)
                .filter(uc_user_id.eq(uid))
                .filter(id.eq(report_id))
                .select((id, customer_id, timestamp, json, referer));
            /* println!("sql: {:?}", query); */

            query.load::<Report>(&conn)
        })
        .await
        .ok()?;
    if result.len() != 1 {
        return None;
    }
    result.get(0).map(|r| r.clone())
}

pub(crate) async fn get_all_reports(
    repo: &Repo,
    user: &User,
    customer_key: &String,
    query_params: &crate::utils::QueryStringAllReportsExtractor,
) -> Option<Vec<Report>> {
    let customer_key: &'static String = unsafe { std::mem::transmute(customer_key) };
    let query_params_all = query_params.all;

    use crate::schema::customers::dsl::{api_key as c_api_key, customers, id as c_id};
    use crate::schema::users_customers::dsl::{
        //customer_id as uc_customer_id,
        user_id as uc_user_id,
        users_customers,
    };

    let uid = user.id;

    let allowed_customer_id = {
        //use crate::schema::customers::dsl::*;
        // customers::belonging_to(&user);
        repo.run(move |conn| {
            let query = customers
                .inner_join(users_customers)
                .filter(uc_user_id.eq(uid))
                .filter(c_api_key.eq(customer_key))
                .select(c_id);
            /* println!(
                "sql(auth): {}",
                diesel::debug_query::<diesel::sqlite::Sqlite, _>(&query)
            ); */

            //query.load::<i32>(&conn)
            query.first::<i32>(&conn)
        })
        .await
        .ok()?
    };

    {
        use crate::schema::reports::dsl::*;
        //use diesel::dsl::now;
        repo.run(move |conn| {
            let mut query = reports
                .filter(customer_id.eq(allowed_customer_id))
                .into_boxed();
            if query_params_all == false {
                let past_time = Utc::now() - Duration::days(30);
                query = query
                    //.filter(timestamp.gt(now - 90.days()))
                    //.select((id, customer_id, timestamp, json, referer))
                    .filter(timestamp.gt(past_time.naive_local()));
            }

            /* println!(
                "sql(reports): {}",
                diesel::debug_query::<diesel::sqlite::Sqlite, _>(&query)
            ); */

            query.load::<Report>(&conn)
        })
        .await
        .ok()
    }

    /*
    // select reports.id,reports.customer_id,users_customers.user_id from reports
    // INNER JOIN customers, users_customers
    // ON reports.customer_id=customers.id AND users_customers.customer_id=customers.id
    // WHERE users_customers.user_id = 1 AND customers.api_key = "OOOOOOO";
    let result = repo
        .run(move |conn| {
            let query = reports
                .inner_join(customers)
                .inner_join(users_customers)
                .filter(uc_user_id.eq(uid))
                .filter(c_api_key.eq(customer_key))
                .select((id, customer_id, timestamp, json, referer));
            println!(
                "sql: {}",
                diesel::debug_query::<diesel::sqlite::Sqlite, _>(&query)
            );

            query.load::<Report>(&conn)
        })
        .await
        .ok()?;

    Some(result)
        */
}

pub(crate) async fn get_all_customers(repo: &Repo, user: &User) -> Option<Vec<Customer>> {
    let uid = user.id;

    use crate::schema::customers::dsl::{customers, *};
    use crate::schema::users_customers::dsl::{
        //customer_id as uc_customer_id,
        user_id as uc_user_id,
        users_customers,
    };

    let results = repo
        .run(move |conn| {
            let query = customers
                .inner_join(users_customers)
                .filter(uc_user_id.eq(uid))
                //.filter(customer_id.eq(allowed_customer_id))
                .select((id, name, api_key, created_at, updated_at));
            /* println!(
                "sql(customers): {}",
                diesel::debug_query::<diesel::sqlite::Sqlite, _>(&query)
            ); */

            query.load::<Customer>(&conn)
        })
        .await;

    results.ok()
}
