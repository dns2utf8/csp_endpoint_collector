//! Holds the two possible structs that are `Queryable` and
//! `Insertable` in the DB
use chrono::NaiveDateTime;
use diesel::{Insertable, Queryable};
use serde::{Deserialize, Serialize};

use crate::schema::reports;

/// Represents a report in the DB.
/// It is `Queryable`
#[derive(Queryable, Serialize, Deserialize, Debug, Clone)]
pub struct Report {
    pub id: i32,
    pub customer_id: i32,
    pub timestamp: NaiveDateTime,
    pub json: String,
    pub referer: String,
}

/// Represents a new report to insert in the DB.
#[derive(Insertable, Deserialize, Debug)]
#[table_name = "reports"]
pub struct NewReport {
    pub customer_id: i32,
    pub json: String,
    pub referer: String,
}

/// Represents a customer in the DB.
/// It is `Queryable`
#[derive(Queryable, Deserialize, Debug)]
pub struct Customer {
    pub id: i32,
    pub name: String,
    pub api_key: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Queryable, Deserialize, Debug, Clone)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub login_key: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}
