#![allow(unused)]

use gotham::handler::{HandlerError, HandlerFuture, MapHandlerError, MapHandlerErrorFuture};
use gotham::helpers::http::response::create_response;
use gotham::hyper::{body, header::HeaderMap, Body, Response, StatusCode};
use gotham::router::response::extender::ResponseExtender;
use gotham::state::{FromState, State};
use gotham_derive::{StateData, StaticResponseExtender};
use hyper::header::{HeaderValue, CACHE_CONTROL};
use mime::TEXT_HTML_UTF_8;
use serde::{Deserialize, Serialize};
use std::{convert::AsRef, todo};

pub struct NotFoundExtender;
///Define an `ResponseExtender`.
///
///Provides a callback after all other processing has  happend
impl ResponseExtender<Body> for NotFoundExtender {
    fn extend(&self, _state: &mut State, response: &mut Response<Body>) {
        *response.body_mut() = err400_body(response.status(), "¯\\(ツ)/¯").into();
    }
}

#[derive(Serialize)]
pub struct RowsUpdated {
    pub rows: usize,
}
#[derive(Serialize)]
pub struct CurrentCsp<'a> {
    pub uri: &'a str,
    pub policy: String,
}
#[derive(Clone, Deserialize, StateData, StaticResponseExtender)]
pub struct CustomerIdExtractor {
    pub customer: String,
}
/*
impl CustomerIdExtractor {
    pub fn is_possible(&self) -> Result<Self, &'static str> {
        let mut all_length = 0usize;
        for c in self.customer.chars() {
            match c {
                '-' => { /* skip */ }
                _c @ '0'..='9' | _c @ 'a'..='f' => all_length += 1,
                _ => {
                    return Err("invalid character");
                }
            }
        }
        if all_length > 128 / 4 {
            Err("input too long")
        } else {
            Ok(self.clone())
        }
    }
}
*/
#[derive(Clone, Deserialize, StateData, StaticResponseExtender)]
pub struct UserIdExtractor {
    pub user: String,
}
#[derive(Clone, Deserialize, StateData, StaticResponseExtender)]
pub struct UserCustomerIdExtractor {
    pub user: String,
    pub customer: String,
}
#[derive(Clone, Deserialize, StateData, StaticResponseExtender)]
pub struct UserReportIdExtractor {
    pub user: String,
    pub report: String,
}

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct QueryStringAllReportsExtractor {
    #[serde(default = "false_fn")]
    pub all: bool,
}
fn false_fn() -> bool {
    false
}

pub fn bad_request<S: AsRef<str>>(
    state: State,
    code: StatusCode,
    body: S,
) -> (State, Response<Body>) {
    let mut response = create_response(
        &state,
        code,
        TEXT_HTML_UTF_8,
        err400_body(code, body.as_ref()),
    );
    response_no_cache(&mut response);

    (state, response)
}

pub fn err400_body(code: StatusCode, response: &str) -> String {
    format!(
        r#"<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,user-scalable=yes"/>
        <title>{num} {label} - {reason}</title>
        <link rel="stylesheet" type="text/css" href="/static/basic.css"  />
    </head>
    <body class="centering">
        <section>
            <h1>{num} {label} - {reason}</h1>
            <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/{num}">
                <h2>Check the MDN docs</h2>
            </a>
            <a href="https://estada.ch/">
                <h3>Contact me</h3>
            </a>
        </section>
    </body>
</html>
"#,
        num = code.as_str(),
        label = code
            .canonical_reason()
            .expect("unable to translate HTTP StatusCode"),
        reason = response,
    )
}

/// Add cache control header to response
pub fn response_no_cache(res: &mut Response<Body>) -> () {
    res.headers_mut()
        .insert(CACHE_CONTROL, HeaderValue::from_static("no-cache"));
}
