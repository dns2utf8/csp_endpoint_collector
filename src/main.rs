//! An example application working with the diesel middleware.

#[macro_use]
extern crate diesel;

#[cfg(test)]
#[macro_use]
extern crate diesel_migrations;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
//use futures::{future, Future, Stream};
use gotham::handler::assets::FileOptions;
//use gotham::handler::{HandlerError, HandlerFuture, IntoHandlerError};
use futures::prelude::*;
use gotham::handler::{HandlerError, HandlerFuture, MapHandlerError, MapHandlerErrorFuture};
use gotham::helpers::http::response::create_response;
use gotham::hyper::{body, header::HeaderMap, Body, Response, StatusCode};
use gotham::pipeline::{new_pipeline, single::single_pipeline};
use gotham::router::{builder::*, Router};
use gotham::state::{FromState, State};
use gotham_middleware_diesel::DieselMiddleware;
use parse_int::parse as parse_int;
use serde_json::Value;
use std::{
    collections::HashMap,
    //use std::io::{Error, ErrorKind};
    path::PathBuf,
    pin::Pin,
    str::from_utf8,
};

mod db;
mod models;
mod schema;
pub mod utils;

use models::NewReport;
use schema::reports;
use utils::{bad_request, err400_body};

// For this example, we'll use a static database URL,
// although one might commonly pass this in via
// environment variables instead.
static DEFAULT_DATABASE_URL: &str = "reports.db";

// We'll use a file based Sqlite database to keep things simple.
// Don't forget to run the step in the README to create the database
// first using the diesel cli.
// For convenience, we define a type for our app's database "Repo",
// with `SqliteConnection` as it's connection type.
pub type Repo = gotham_middleware_diesel::Repo<SqliteConnection>;

/// get("/reports/:user/:customer?all=true")
fn get_reports(state: State) -> Pin<Box<HandlerFuture>> {
    //use crate::schema::reports::dsl::*;

    let repo = Repo::borrow_from(&state).clone();
    async move {
        let uc = utils::UserCustomerIdExtractor::borrow_from(&state);

        let user = match db::get_user(&repo, &uc.user).await {
            Some(u) => u,
            None => return Ok(bad_request(state, StatusCode::UNAUTHORIZED, "invalid user")),
        };

        let query_param = utils::QueryStringAllReportsExtractor::borrow_from(&state);

        let result = db::get_all_reports(&repo, &user, &uc.customer, &query_param).await;
        match result {
            Some(users) => {
                let body = serde_json::to_string(&users).expect("Failed to serialize users.");
                let mut res = create_response(&state, StatusCode::OK, mime::APPLICATION_JSON, body);
                utils::response_no_cache(&mut res);
                Ok((state, res))
            }
            //Err(e) => Err((state, e.into())),
            None => {
                return Ok(bad_request(
                    state,
                    StatusCode::UNAUTHORIZED,
                    "You are not allowed to fetch these reports",
                ))
            }
        }
    }
    .boxed()
}

/// post("/report/:customer")
fn create_report(mut state: State) -> Pin<Box<HandlerFuture>> {
    let repo = Repo::borrow_from(&state).clone();
    async move {
        let customer = utils::CustomerIdExtractor::borrow_from(&state);
        let customer_id = db::get_customer_id(&repo, customer).await;

        let customer_id = match customer_id {
            Ok(id) => id,
            Err(reason) => {
                return Ok(bad_request(
                    state,
                    StatusCode::FORBIDDEN,
                    reason, // "invalid customer id",
                            // "you are not allowed to create a report"
                ));
            }
        };

        let headers = {
            let headers = state
                .try_borrow::<HeaderMap>()
                .expect("unable to get headers")
                .iter()
                .map(|(k, v)| {
                    (
                        k.to_string(),
                        v.to_str().expect("header non-ascii").to_string(),
                    )
                })
                .collect::<HashMap<String, String>>();
            //println!("{:#?}", headers);
            serde_json::to_string(&headers).expect("unable to serialize HeaderMap")
        };

        let report = match extract_json::<Value>(&mut state).await {
            Ok(report) => {
                if report["csp-report"].is_object() {
                    NewReport {
                        customer_id,
                        json: report.to_string(),
                        referer: headers,
                    }
                } else {
                    let body = err400_body(StatusCode::BAD_REQUEST, "no csp-report field");
                    let e = create_response(
                        &state,
                        StatusCode::BAD_REQUEST,
                        mime::APPLICATION_JSON,
                        body,
                    );
                    return Ok((state, e));
                }
            }
            Err(e) => {
                use gotham::handler::IntoResponse;
                let mut response = e.into_response(&state);
                *response.body_mut() = err400_body(response.status(), "invalid JSON body").into();
                return Ok((state, response));
                //return Err((state, e))
            }
        };

        let query_result = repo
            .run(move |conn| {
                diesel::insert_into(reports::table)
                    .values(&report)
                    .execute(&conn)
            })
            .await;

        let rows = match query_result {
            Ok(rows) => rows,
            Err(e) => return Err((state, e.into())),
        };

        let body = serde_json::to_string(&utils::RowsUpdated { rows })
            .expect("Failed to serialise to json");
        let res = create_response(&state, StatusCode::CREATED, mime::APPLICATION_JSON, body);

        Ok((state, res))
    }
    .boxed()
}

/// get("/check/:user/:report")
fn get_check(state: State) -> Pin<Box<HandlerFuture>> {
    let repo = Repo::borrow_from(&state).clone();
    async move {
        let ur = utils::UserReportIdExtractor::borrow_from(&state);
        let report_id = match parse_int(&ur.report) {
            Ok(id) => id,
            Err(e) => {
                return Ok(bad_request(
                    state,
                    StatusCode::BAD_REQUEST,
                    format!("invalid report id: {:?}", e),
                ))
            }
        };

        let user = match db::get_user(&repo, &ur.user).await {
            Some(u) => u,
            None => return Ok(bad_request(state, StatusCode::UNAUTHORIZED, "invalid user")),
        };

        let report = match db::get_report(&repo, &user, report_id).await {
            Some(r) => r,
            None => {
                return Ok(bad_request(
                    state,
                    StatusCode::NOT_FOUND,
                    "unable to find report",
                ))
            }
        };

        let json =
            serde_json::from_str::<Value>(&report.json).expect("unable to restore generated JSON");

        let uri = json.pointer("/csp-report/document-uri");
        let uri = match uri.and_then(Value::as_str) {
            Some(u) => u,
            None => {
                return Ok(bad_request(
                    state,
                    StatusCode::NOT_ACCEPTABLE,
                    "unable to restore document-uri from report",
                ))
            }
        };

        let response = reqwest::get(uri).await;
        let policy = match response {
            Err(e) => format!("Error: {}", e.to_string()),
            Ok(response) => {
                let headers = response.headers();
                let csp = headers
                    .get(reqwest::header::CONTENT_SECURITY_POLICY)
                    .map(|c| c.to_str().unwrap_or("Unable to decode CSP"))
                    .unwrap_or("NO CSP supplied!");
                format!("{}", csp)
            }
        };
        let body = serde_json::to_string(&utils::CurrentCsp { uri, policy })
            .expect("Failed to serialise to json");
        let mut res = create_response(&state, StatusCode::OK, mime::APPLICATION_JSON, body);
        utils::response_no_cache(&mut res);

        Ok((state, res))
    }
    .boxed()
}

fn get_analyser(state: State) -> Pin<Box<HandlerFuture>> {
    let repo = Repo::borrow_from(&state).clone();
    async move {
        let u = utils::UserIdExtractor::borrow_from(&state);
        let user = match db::get_user(&repo, &u.user).await {
            Some(u) => u,
            None => return Ok(bad_request(state, StatusCode::UNAUTHORIZED, "invalid user")),
        };

        let customers = match db::get_all_customers(&repo, &user).await {
            Some(c) => c,
            None => {
                return Ok(bad_request(
                    state,
                    StatusCode::NOT_FOUND,
                    "No Customers linked to your account",
                ))
            }
        };

        let mut html = String::new();
        for c in &customers {
            html += &format!(
                "\n    <label><input type=\"checkbox\" value=\"{}\"/>{}</label>",
                c.api_key, c.name,
            );
        }

        let template = include_str!("../static/analyzer.html");
        let body = template
            .replace("{{ customer_panel }}", &html)
            .replace("{{ server_version }}", env!("CARGO_PKG_VERSION"));

        let res = create_response(&state, StatusCode::OK, mime::TEXT_HTML_UTF_8, body);
        Ok((state, res))
    }
    .boxed()
}

fn get_version(state: State) -> (State, Response<Body>) /*Pin<Box<HandlerFuture>>*/ {
    let body = env!("CARGO_PKG_VERSION");
    let res = create_response(&state, StatusCode::OK, mime::TEXT_HTML_UTF_8, body);
    //async move { Ok((state, res)) }.boxed()
    (state, res)
}

fn router(repo: Repo) -> Router {
    // Add the diesel middleware to a new pipeline
    let (chain, pipeline) =
        single_pipeline(new_pipeline().add(DieselMiddleware::new(repo)).build());

    // Build the router
    build_router(chain, pipeline, |route| {
        route.get("/").to_file("./static/index.html");
        route.get("/version").to(get_version);
        route.get("/static/*").to_dir(
            FileOptions::new(PathBuf::from("./static/"))
                .with_cache_control("no-cache")
                // Disable because it does not support on the fly compression
                // .with_gzip(true)
                // .with_brotli(true)
                .build(),
        );

        route
            .get("/analyse/:user")
            .with_path_extractor::<utils::UserIdExtractor>()
            .to(get_analyser);
        route
            .get("/analyse/:user/")
            .with_path_extractor::<utils::UserIdExtractor>()
            .to(get_analyser);

        route
            .get("/reports/:user/:customer")
            .with_path_extractor::<utils::UserCustomerIdExtractor>()
            .with_query_string_extractor::<utils::QueryStringAllReportsExtractor>()
            .to(get_reports);

        // incoming reports
        route
            .post("/report/:customer")
            .with_path_extractor::<utils::CustomerIdExtractor>()
            .to(create_report);

        route.get("/report").to_file("static/index.html");
        route.get("/report/").to_file("static/index.html");

        route
            .get("/check/:user/:report")
            .with_path_extractor::<utils::UserReportIdExtractor>()
            .to(get_check);

        route.add_response_extender(StatusCode::NOT_FOUND, utils::NotFoundExtender);
    })
}

async fn extract_json<T>(state: &mut State) -> Result<T, HandlerError>
where
    T: serde::de::DeserializeOwned,
{
    let body = body::to_bytes(Body::take_from(state))
        .map_err_with_status(StatusCode::BAD_REQUEST)
        .await?;
    let b = body.to_vec();
    from_utf8(&b)
        .map_err_with_status(StatusCode::BAD_REQUEST)
        .and_then(|s| serde_json::from_str::<T>(s).map_err_with_status(StatusCode::BAD_REQUEST))
}

/// Start a server and use a `Router` to dispatch requests
fn main() {
    dotenv::dotenv().ok();
    let mut addr = std::env::var("LISTEN").unwrap_or("http://[::1]:7878".into());
    let database_url = std::env::var("DEFAULT_DATABASE_URL").unwrap_or(DEFAULT_DATABASE_URL.into());

    if let Ok(content) = std::fs::read_to_string("backend_url.txt") {
        println!("using backend_url.txt");
        addr = content.trim().to_string();
    }

    println!("Listening for requests at {} use LISTEN to override", addr);
    let addr = url_to_addr(addr);
    println!("Using {:?} as DEFAULT_DATABASE_URL", database_url);
    gotham::start(addr, router(Repo::new(&database_url)));
}

fn url_to_addr<S: AsRef<str>>(input: S) -> std::net::SocketAddr {
    //use url::{Host, Position, Url};
    let url = url::Url::parse(input.as_ref()).expect("invalid input");

    let brackets = |c| c == '[' || c == ']';
    let host_str = url
        .host_str()
        .expect("unable to get host_str")
        .trim_matches(&brackets);

    std::net::SocketAddr::new(
        host_str.parse().expect("non-numeric host part"),
        url.port().unwrap_or(7878),
    )
}

// In tests `Repo::with_test_transactions` allows queries to run
// within an isolated test transaction. This means multiple tests
// can run in parallel without trampling on each other's data.
// This isn't necessary when using an SQLite in-memory only database
// as is used here, but is demonstrated here anyway to show how it
// might be used agaist a real database.
#[cfg(test)]
mod tests {
    use super::*;
    use failure::Fail;
    use gotham::test::TestServer;
    use gotham_middleware_diesel::Repo;
    use hyper::StatusCode;
    use std::str;
    use tokio::runtime::Runtime;

    static DEFAULT_DATABASE_URL: &str = ":memory:";

    // For this example, we run migrations automatically in each test.
    // You could also choose to do this separately using something like
    // `cargo-make` (https://sagiegurari.github.io/cargo-make/) to run
    // migrations before the test suite.
    embed_migrations!();

    #[test]
    fn parse_backend_url() {
        let input = "http://[::1]:4000/\n";
        let output = url_to_addr(input);

        let expected = std::net::SocketAddr::new("::1".parse().unwrap(), 4000);

        assert_eq!(expected, output);
    }

    #[test]
    fn get_empty_reports() {
        let repo = Repo::with_test_transactions(DEFAULT_DATABASE_URL);
        let runtime = Runtime::new().unwrap();
        //runtime::run(repo.run(|conn| embedded_migrations::run(&conn).map_err(|_| ())));
        let _ = runtime
            .block_on(repo.run(|conn| embedded_migrations::run(&conn).map_err(|e| e.compat())));
        let test_server = TestServer::new(router(repo)).unwrap();
        let response = test_server
            .client()
            .get("http://localhost/reports")
            .perform()
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);

        let body = response.read_body().unwrap();
        let str_body = str::from_utf8(&body).unwrap();
        let index = "[]";
        assert_eq!(str_body, index);
    }

    #[test]
    //#[allow_fail]
    fn create_and_retrieve_report() {
        let repo = Repo::with_test_transactions(DEFAULT_DATABASE_URL);
        let runtime = Runtime::new().unwrap();
        //runtime::run(repo.run(|conn| embedded_migrations::run(&conn).map_err(|_| ())));
        let _ = runtime
            .block_on(repo.run(|conn| embedded_migrations::run(&conn).map_err(|e| e.compat())));
        let test_server = TestServer::new(router(repo)).unwrap();

        //  First we'll insert something into the DB with a post
        let body = r#"{"title":"test","price":1.0,"link":"http://localhost"}"#;
        let response = test_server
            .client()
            .post("http://localhost/reports", body, mime::APPLICATION_JSON)
            .perform()
            .unwrap();
        assert_eq!(response.status(), StatusCode::CREATED);

        // Then we'll query it and test that it is returned
        // As long as we're hitting a `test_server` created with the same
        // `Repo` instance, we're in the same test transaction, and our
        // data will be there across queries.
        let response = test_server
            .client()
            .get("http://localhost/reports")
            .perform()
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);

        let body = response.read_body().unwrap();
        let str_body = str::from_utf8(&body).unwrap();
        let index = r#"[{"id":1,"title":"test","price":1.0,"link":"http://localhost"}]"#;
        assert_eq!(str_body, index);
    }
}
