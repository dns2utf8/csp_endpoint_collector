table! {
    reports (id) {
        id -> Integer,
        customer_id -> Integer,
        timestamp -> Timestamp,
        json -> Text,
        referer -> Text,
    }
}

table! {
    customers (id) {
        id -> Integer,
        name -> Text,
        api_key -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Integer,
        username -> Text,
        login_key -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    users_customers (id) {
        id -> Integer,
        user_id -> Integer,
        customer_id -> Integer,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(reports -> customers (customer_id));
joinable!(users_customers -> customers (customer_id));
joinable!(users_customers -> users (user_id));

// TODO: this is wrong
joinable!(reports -> users_customers (customer_id));

allow_tables_to_appear_in_same_query!(users_customers, reports, users, customers);
